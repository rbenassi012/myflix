import { configureStore  } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import  omdbReducer  from '@myflix/reducers/omdb'
import authenticationReducer from "@myflix/reducers/authentication";

const reducer = combineReducers({
    authentication: authenticationReducer,
    omdb: omdbReducer,
})

const store = configureStore({
    reducer,
});

export default store;
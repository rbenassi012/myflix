import action from './action.png';
import adventure from './adventure.png';
import animation from './animation.png';
import comedy from './comedy.png';
import crime from './crime.png';
import documentary from './documentary.png';
import drama from './drama.png';
import family from './family.png';
import fantasy from './fantasy.png';
import horror from './horror.png';
import history from './history.png';
import mystery from './mystery.png';
import music from './music.png';
import romance from './romance.png';
import thriller from './thriller.png';
import war from './war.png';
import western from './western.png';

import popular from './popular.png';
import upcoming from './upcoming.png';

export default {
  action,
  adventure,
  animation,
  comedy,
  crime,
  documentary,
  drama,
  family,
  fantasy,
  horror,
  history,
  mystery,
  music,
  romance,
  thriller,
  war,
  western,
  popular,
  upcoming,
};

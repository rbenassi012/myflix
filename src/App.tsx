import MyFlixRoutes from '@myflix/routes';
import ThemeCustomization from '@myflix/themes';

function App() {

  return (
    <ThemeCustomization>
      <MyFlixRoutes />
    </ThemeCustomization>
  )
}

export default App
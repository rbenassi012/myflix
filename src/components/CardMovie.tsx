// material ui
import { makeStyles } from '@material-ui/core/styles';
import { Box, Grow } from '@mui/material';

// navigation
import { Link } from 'react-router-dom';

// project import
import { Movie } from '@myflix/types/movie';

const useStyles = makeStyles((theme) => ({
    movie: {
      padding: '15px',
    },
    links: {
      alignItems: 'center',
      fontWeight: 'bolder',
      textDecoration: 'none',
      [theme.breakpoints.up('xs')]: {
        display: 'flex',
        flexDirection: 'column',
      },
      '&:hover': {
        cursor: 'pointer',
      },
    },
    imageContainer: {
        backgroundColor: theme.palette.common.black, 
        height: 280,
        overflow: 'hidden',
        borderRadius: theme.spacing(1),
    },
    image: {
     
      maxWidth: 200,
      maxHeight: 280,
      marginBottom: 10,
      '&:hover': {
        transform: 'scale(1.05)',
      },
      [theme.breakpoints.down('sm')]: {
        height: 150,
      },
    },
  }));


interface CardMovieProps {
    movie: Movie,
    i: number,
}


function CardMovie(props: CardMovieProps) {
  const {movie, i} = props;
  const classes = useStyles();

  return (
    <Box className={classes.movie}>
      <Grow in key={i} timeout={(i + 1) * 350}>
        <Link className={classes.links} to={`/movie/${movie.imdbID}`}>
          <Box className={classes.imageContainer}>
          <img
            src={movie.Poster}
            alt={movie.Title}
            className={classes.image}
          />
          </Box>
        </Link>
      </Grow>
    </Box>
  );
}

export default CardMovie;
// material-ui
import Box from '@material-ui/core/Box';

// third part
import Slider from 'react-slick';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

// project import
import { Movie } from '@myflix/types/movie';
import CardMovie from '@myflix/components/CardMovie'


interface MoviesProps {
    movies: Movie[],
    visible: number,
    scroll: number,
}

const Movies = (props: MoviesProps) => {
    const { movies, visible, scroll } = props;

    const settings = {
        infinite: true,
        slidesToShow: visible, // Numero di film visibili alla volta
        slidesToScroll: scroll,
        prevArrow: <button className="slider-arrow prev" />,
        nextArrow: <button className="slider-arrow next" />,
    };

    return (
        <Box style={{ width: 'calc(100% - 60px)', marginLeft: 25 }}>
            <Slider {...settings}>
                {movies.map((movie, i) => (
                    <CardMovie key={i} movie={movie} i={i} />
                ))}
            </Slider>
        </Box>
    );
}

export default Movies;
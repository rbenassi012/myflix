

// third-party
import { motion } from 'framer-motion';

// ==============================|| ANIMATED BUTTON ||============================== //

enum AnimationType {
    ROTATE = 'rotate',
    SLIDE = 'slide',
    SCALE = 'scale',
}

interface AnimatedButtonProps {
    type?: AnimationType,
    children: JSX.Element,
}

export default function AnimatedButton(props: AnimatedButtonProps) {
  const {type, children} = props;
  switch (type) {
    //TODO to be implemented the different types of animation
    default:
      return (
        <motion.div whileHover={{ scale: 1 }} whileTap={{ scale: 0.9 }}>
          {children}
        </motion.div>
      );
  }
}

import * as React from 'react';

// material ui imports
import { useTheme } from '@mui/material/styles';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import Avatar from '@mui/material/Avatar';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import HomeIcon from '@material-ui/icons/Home';

// third path
import { useNavigate } from 'react-router-dom';
import { useDispatch} from 'react-redux'


// project imports
import Search from '@myflix/components/Search'
import { LOGOUT_PAGE_PATH, HOME_PAGE_PATH } from '../routes/consts';
import { setLoginStatus } from '@myflix/reducers/authentication';

// assets
import Teaser from '@myflix/components/Teaser';



interface NavbarProps {
    settings: string[];
    goToHome: boolean;
}


const Navbar = (props: NavbarProps) => {
    const { settings, goToHome } = props;
    const theme = useTheme();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(null);

    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseUserMenu = (action?: string) => {
        setAnchorElUser(null);

        switch (action) {
            case 'Logout':
                dispatch(setLoginStatus("false"))
                navigate(LOGOUT_PAGE_PATH)
                break;
            case 'home':
                navigate(HOME_PAGE_PATH)
        }


    };

    return (
        <AppBar position="static" style={{ backgroundColor: theme.palette.background.default }}>
            <Box ml={1} mr={1}>
                <Toolbar disableGutters>
                    <Teaser width={200} />
                    <Box sx={{ flexGrow: 1 }} />
                    {goToHome && (<Box>
                        <IconButton onClick={() => handleCloseUserMenu('home')} sx={{ p: 0, m: 2 }}>
                            <HomeIcon
                                style={{ color: theme.palette.common.white }}
                                alt="go to home"
                            />
                        </IconButton>
                    </Box>)}
                    <Box mr={2}> <Search /></Box>
                    <Box sx={{ flexGrow: 0 }}>
                        <Tooltip title="Open settings">
                            <Box color={theme.palette.common.white}>
                                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                    <Avatar style={{ background: theme.palette.background.default, color: theme.palette.common.white }} alt="Remy Sharp" />
                                </IconButton>
                            </Box>
                        </Tooltip>
                        <Menu
                            sx={{ mt: '45px' }}
                            id="menu-appbar"
                            anchorEl={anchorElUser}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorElUser)}
                            onClose={() => handleCloseUserMenu}
                        >
                            {settings.map((setting) => (
                                <MenuItem key={setting} onClick={() => handleCloseUserMenu(setting)}>
                                    <Typography textAlign="center">{setting}</Typography>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>
                </Toolbar>
            </Box>
        </AppBar>
    );
}
export default Navbar;
import { forwardRef } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Card, CardContent, CardHeader, Typography } from '@mui/material';


// header style
const headerSX = {
    p: 2.5,
    '& .MuiCardHeader-action': { m: '0px auto', alignSelf: 'center' }
};

// ==============================|| CUSTOM - BASE CARD ||============================== //

interface BaseCardProps {
    border: boolean,
    boxShadow: boolean,
    contentSX: any,
    content: boolean,
    darkTitle: boolean,
    divider: boolean,
    elevation: number,
    secondary: JSX.Element,
    shadow: string,
    sx: any,
    title: string | JSX.Element,
    codeHighlight: boolean,
    children: JSX.Element,
};


const BaseCard = forwardRef(
    (
        props: BaseCardProps,
        ref
    ) => {
        const theme = useTheme();
        const {
            boxShadow,
            border,
            children,
            content,
            contentSX,
            darkTitle,
            elevation,
            secondary,
            shadow,
            sx,
            title,
            codeHighlight,
            ...others
        } = props
        const bxShadow = theme.palette.mode === 'dark' ? boxShadow || true : boxShadow;

        return (
            <Card
                elevation={elevation || 0}
                ref={ref}
                {...others}
                sx={{
                    border: border ? '1px solid' : 'none',
                    borderRadius: 2,
                    borderColor: theme.palette.mode === 'dark' ? theme.palette.divider : theme.palette.grey.A800,
                    boxShadow: bxShadow && (!border || theme.palette.mode === 'dark') ? shadow || theme.customShadows.z1 : 'inherit',
                    ':hover': {
                        boxShadow: bxShadow ? shadow || theme.customShadows.z1 : 'inherit'
                    },
                    '& pre': {
                        m: 0,
                        p: '16px !important',
                        fontFamily: theme.typography.fontFamily,
                        fontSize: '0.75rem'
                    },
                    ...sx
                }}
            >
                {!darkTitle && title && (
                    <CardHeader sx={headerSX} titleTypographyProps={{ variant: 'subtitle1' }} title={title} action={secondary} />
                )}
                {darkTitle && title && <CardHeader sx={headerSX} title={<Typography variant="h3">{title}</Typography>} action={secondary} />}

                {content && <CardContent sx={contentSX}>{children}</CardContent>}

            </Card>
        );
    }
);



export default BaseCard;
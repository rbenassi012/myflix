// material-ui
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';

// assets
import { ReactComponent as Logo } from '@myflix/assets/images/Logo.svg';

// ==============================|| TEASER - MAIN LOGO ||============================== //

interface TeaserProps {
    width?: number 
}

const Teaser = (props: TeaserProps) => {
    const theme = useTheme();
    const { width } = props;
    return (<>
        <Box ml={3}>
            <Logo width={width !== undefined ? width: 400} />
        </Box>
        <Box color={theme.palette.getContrastText(theme.palette.background.default)}>My movies. My home.</Box>
    </>
    )
}

export default Teaser;

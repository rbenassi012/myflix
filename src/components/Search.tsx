import { useState } from 'react';
// material ui 
import {
    OutlinedInput,
    InputAdornment,
    IconButton
} from '@mui/material';
import SearchIcon from '@material-ui/icons/Search';

import { useTheme } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';
import { HOME_PAGE_PATH } from '../routes/consts';

function Search() {
    const theme = useTheme();
    const [query, setQuery] = useState('');
    const navigate = useNavigate()

    const handleKeyPress = (e) => {
        if (e.key === "Enter") {
           navigate(`${HOME_PAGE_PATH}?search=${query}`)
        }
    }

    const handleSearchButton = () => {
        navigate(`${HOME_PAGE_PATH}?search=${query}`)
    }

    return (
        <>
            <OutlinedInput
                id="search"
                type="search"
                name="search"
                placeholder="Search movies"
                fullWidth
                size="small"
                autoComplete="email"
                value={query}
                onChange={(e) => setQuery(e.target.value)}
                onKeyUp={handleKeyPress}
                inputProps={{
                    style: {
                        color: theme.palette.getContrastText(theme.palette.background.default)
                    }
                }}
                endAdornment={
                    <InputAdornment position="end">
                        <IconButton
                            aria-label=""
                            edge="start"
                            size="small"
                            onClick={handleSearchButton}
                        >
                            <SearchIcon />
                        </IconButton>
                    </InputAdornment>
                }

            />
        </>
    );
}

export default Search;
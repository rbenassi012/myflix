
import { makeStyles } from '@material-ui/core/styles';
import { Box, Typography, Card, CardContent, CardMedia, Zoom, CircularProgress } from '@mui/material';

// navigation
import { Link } from 'react-router-dom';

// project import
import { MovieDetails } from '@myflix/types/movie';


const useStyles = makeStyles((theme) => ({
  featuredCardContainer: {
    marginBottom: '20px',
    display: 'flex',
    justifyContent: 'center',
    height: '490px',
    textDecoration: 'none',
  },
  card: {
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
    flexDirection: 'column',
  },
  cardRoot: {
    position: 'relative',
  },
  cardMedia: {
    position: 'absolute',
    top: 0,
    right: 0,
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(0,0,0,0.575)',
    backgroundBlendMode: 'darken',
  },
  cardContent: {
    width: '40%',
    float: "right",
    padding: theme.spacing(4),
    borderRadius: theme.spacing(1),
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
    backgroundColor: theme.palette.primary.dark,
    opacity: 0.9,
  },
  cardContentRoot: {
    position: 'relative',
    color: theme.palette.getContrastText(theme.palette.primary.main),
    minHeight: 200,
  },
}));


interface FeaturedMovieProps {
  movie: MovieDetails,
  isLoading: boolean,
}

const FeaturedMovie = (props: FeaturedMovieProps) => {
  const classes = useStyles();
  const { movie, isLoading } = props;

  if (isLoading) return (
    <Box className={classes.featuredCardContainer}>
       <Card className={classes.card} classes={{ root: classes.cardRoot }}>
      <CircularProgress />
      </Card>
    </Box>
  )
  if (movie === null) {
    return null
  }

  return (
    <Box component={Link} to={`/movie/${movie.imdbID}`} className={classes.featuredCardContainer}>
      <Zoom in={true}>
        <Card className={classes.card} classes={{ root: classes.cardRoot }}>
          <CardMedia
            component="img"
            image={`${movie.Poster.replace("SX300.jpg", "SX1200.jpg")}`}
            title={movie.Title}
            className={classes.cardMedia}
          />
          <Box padding="20px">
            <CardContent className={classes.cardContent} classes={{ root: classes.cardContentRoot }}>
              <Typography variant="h2" gutterBottom>{movie.Title}</Typography>
              <Typography variant="body1">{movie.Plot}</Typography>
              <br />
              <Typography variant="body2">{movie.Country} - {movie.Year}</Typography>
            </CardContent>
          </Box>
        </Card>
      </Zoom>
    </Box>
  );
}

export default FeaturedMovie;
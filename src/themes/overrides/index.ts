// third-party
import { merge } from 'lodash';

// project import
import OutlinedInput from './OutlinedInput';

// ==============================|| OVERRIDES - MAIN ||============================== //

export default function ComponentsOverrides(theme) {
  return merge(
    OutlinedInput(theme),
  );
}
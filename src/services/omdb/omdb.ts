// omdbClient.ts
import axios, { AxiosResponse } from 'axios';
import {Movie, MovieDetails} from '@myflix/types/movie'

const OMDB_API_URL = import.meta.env.VITE_OMDB_API_URL || '';
const OMDB_API_KEY = import.meta.env.VITE_OMDB_API_KEY || '';

class OMDBClient {
  private apiKey: string;
  private apiUrl: string;

  constructor(apiUrl: string, apiKey: string) {
    this.apiKey = apiKey;
    this.apiUrl = apiUrl;
  }

  async searchMovies(query: string, pages: number): Promise<Movie[]> {
    try {
      const response: AxiosResponse = await axios.get(`${this.apiUrl}?apikey=${this.apiKey}&s=${query}&page=${pages}`);
      return response.data.Search || [];
    } catch (error) {
      console.error('Error while searching entries in the OMDB: ', error);
      throw error;
    }
  }

  async getMovieDetails(imdbID: string): Promise<MovieDetails | null> {
    try {
      const response: AxiosResponse = await axios.get(`${this.apiUrl}?apikey=${this.apiKey}&i=${imdbID}`);
      return response.data;
    } catch (error) {
      console.error('Error while getting entry detail: ', error);
      throw error;
    }
  }

  async getMovieByTitle(title: string): Promise<Movie> {
    try {
      const response: AxiosResponse = await axios.get(`${this.apiUrl}?apikey=${this.apiKey}&t=${title}`);
      return response.data;
    } catch (error) {
      console.error('Error while searching entries in the OMDB: ', error);
      throw error;
    }
  }

}

const omdbClient = new OMDBClient(OMDB_API_URL, OMDB_API_KEY);
export default omdbClient;

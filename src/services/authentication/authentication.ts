import { LOGIN_STORAGE_KEY } from "./const";

class AuthenticationClient {

    checkLoginStatus = () => {
        return localStorage.getItem(LOGIN_STORAGE_KEY) === 'true';
    };

    setLoginStatus = (value: string) => {
        localStorage.setItem(LOGIN_STORAGE_KEY, value)
    }
}

const authenticationClient = new AuthenticationClient();
export default authenticationClient;

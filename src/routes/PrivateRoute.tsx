import { useEffect } from 'react';

import { Outlet, Navigate } from 'react-router-dom'
// third part
import { useSelector, useDispatch } from 'react-redux';
import { checkLoginStatus } from '@myflix/reducers/authentication';


interface PrivateRouteProps {
    redirectTo: string,
    children: JSX.Element
}

const PrivateRoute = (props: PrivateRouteProps) => {
    const {redirectTo, children} = props;
    const dispatch = useDispatch();
    useEffect(()  => {
        dispatch(checkLoginStatus())
    }, [])

    const isLogged = useSelector(state => state.authentication.isLogged)
    return (
        <>
            { isLogged ?  children || <Outlet /> : <Navigate to={redirectTo} />}
        </>
    )
}


export default PrivateRoute

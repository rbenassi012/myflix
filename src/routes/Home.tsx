import { lazy } from 'react';

// project import
import Loadable from '@myflix/components/Loadable'

// navigation
import { LOGIN_PAGE_PATH } from './consts';

// render - login
const PrivateRoute = Loadable(lazy(() => import('@myflix/routes/PrivateRoute')));
const Home = Loadable(lazy(() => import('@myflix/pages/home/Home')));

// ==============================|| AUTH ROUTING ||============================== //

const HomeRoutes = {
    path: '/',
    element:
        <PrivateRoute redirectTo={LOGIN_PAGE_PATH}>
            <Home />
        </PrivateRoute>,
};

export default HomeRoutes;
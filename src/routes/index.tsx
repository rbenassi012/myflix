import { useRoutes } from 'react-router-dom';

// project import
import LoginRoutes from './Login';
import NotFoundRoutes from './NotFound';
import Home from './Home';
import Detail from './Detail';


// ==============================|| ROUTING RENDER ||============================== //

export default function MyFlixRoutes() {
  return useRoutes([LoginRoutes, Home, Detail, NotFoundRoutes]);
}
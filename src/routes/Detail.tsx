import { lazy } from 'react';

// project import
import Loadable from '@myflix/components/Loadable'

// navigation
import { LOGIN_PAGE_PATH } from './consts';

// render - login
const PrivateRoute = Loadable(lazy(() => import('@myflix/routes/PrivateRoute')));
const Detail = Loadable(lazy(() => import('@myflix/pages/detail/Detail')));

// ==============================|| AUTH ROUTING ||============================== //

const DetailRoutes = {
    path: '/movie/:imdbID',
    element:
        <PrivateRoute redirectTo={LOGIN_PAGE_PATH}>
            <Detail />
        </PrivateRoute>,
};

export default DetailRoutes;
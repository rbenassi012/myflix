import {useEffect } from 'react'
import { Outlet, Navigate } from 'react-router-dom'
// third part
import { useSelector, useDispatch } from 'react-redux';
import { checkLoginStatus } from '@myflix/reducers/authentication';


interface PublicRouteProps {
    redirectTo: string,
    children: JSX.Element
}

const PublicRoute = (props: PublicRouteProps) => {
    const {redirectTo, children} = props;
    const dispatch = useDispatch();
    useEffect(()  => {
        dispatch(checkLoginStatus())
    }, [])

    const isLogged = useSelector(state => state.authentication.isLogged)
   
    return (
        <>
            { isLogged ?   <Navigate to={redirectTo} /> : children || <Outlet />}
        </>
    )
}


export default PublicRoute

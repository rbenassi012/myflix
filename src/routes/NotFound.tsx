import { lazy } from 'react';

// project import
import Loadable from '@myflix/components/Loadable'

// render - Not Found
const NotFound = Loadable(lazy(() => import('@myflix/pages/navigation/NotFound')));

// ==============================|| AUTH ROUTING ||============================== //

const NotFoundRoutes = {
  path: '*',
  element:  <NotFound />,
};

export default NotFoundRoutes;
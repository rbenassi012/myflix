import { lazy } from 'react';

// project import
import Loadable from '@myflix/components/Loadable'
import { HOME_PAGE_PATH } from './consts';

// render - login
const PublicRoute = Loadable(lazy(() => import('@myflix/routes/PublicRoute')));
const Login = Loadable(lazy(() => import('@myflix/pages/authentication/Login')));

// ==============================|| AUTH ROUTING ||============================== //

const LoginRoutes = {
  path: '/login',
  element:   <PublicRoute redirectTo={HOME_PAGE_PATH}>
  <Login />
</PublicRoute>
};

export default LoginRoutes;
import { useEffect } from 'react';

// third part
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

// material-ui
import { makeStyles } from '@material-ui/core/styles';
import { useTheme } from '@mui/material/styles';
import { Box, Typography, Grid, Button, ButtonGroup, Rating } from '@mui/material';

// project imports
import Navbar from '@myflix/components/Navbar';
import BaseCard from '@myflix/components/BaseCard';
import Movies from '@myflix/components/Movies'

import { getMovieDetails, searchMovies } from '@myflix/reducers/omdb';
import { DEFAULT_NAVBAR_OPTIONS } from '@myflix/routes/consts';

// assets
import genreIcons from '@myflix/assets/images/genres';



// ================================|| DETAIL ||================================ //


const useStyles = makeStyles((theme) => ({
    posterContainer: {
        height: '100%',
        backgroundColor: 'black',
    },
    poster: {
        width: '100%',
    },
    genreImage: {
        marginRight: theme.spacing(2),
        display: 'inline-block',
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        width: '100%',
        [theme.breakpoints.down('sm')]: {
            flexDirection: 'column',
        },
    }

}));


const Detail = () => {
    const theme = useTheme();
    const classes = useStyles();
    const params = useParams();
    const dispatch = useDispatch();


    useEffect(() => {
        dispatch(getMovieDetails(params.imdbID));
    }, [params.imdbID]);

    const movie = useSelector(state => state.omdb.movieDetails)
    const movies = useSelector(state => state.omdb.movies)

    useEffect(() => {
        if (movie !== null) {
            const genre = movie.Genre.split(',')
            if (genre.length > 0) {
                dispatch(searchMovies({ query: genre[0], pages: Math.floor(Math.random() * 10) }))
            }
        }
    }, [movie])



    const addToWatchList = () => {
        alert("Feature not available")
    }

    const viewTrailer = () => {
        alert("Feature not available")
    }


    if (movie === null || movie.imdbID !== params.imdbID) {
        return;
    }

    return (
        <>
            <Navbar
                settings={DEFAULT_NAVBAR_OPTIONS}
                goToHome={true}
            />

            <Box p={3} style={{ clear: "both" }}>
                <BaseCard
                    content={true}
                >
                    <Grid container spacing={2}>
                        <Grid item xs={10}>
                            <Typography variant="h3" align="center" gutterBottom>
                                {movie.Title} - {movie.Year}
                            </Typography>
                            <Box>

                                {movie.Genre.split(',').map((genre: string, index: number) => (
                                    <Box key={genre} display={"flex"} alignItems={"flex-start"}>
                                        {index === 0 && (
                                            <>
                                                <img src={genreIcons[genre.toLowerCase()]} className={classes.genreImage} height={30} />
                                                <Box mt={1}><Typography color="textPrimary" variant="subtitle1">{genre}</Typography></Box>
                                            </>
                                        )
                                        }
                                    </Box>)
                                )}
                                <br />
                                <Box display="flex" mb={2}>
                                    <Typography color="textPrimary" variant="subtitle1">Rating: </Typography> <Rating readOnly value={Number(movie.imdbRating) / 5} />
                                    <Typography gutterBottom variant="subtitle1" style={{ marginRight: "20px", marginLeft: '20px' }}>
                                        {movie.imdbRating}
                                    </Typography>
                                </Box>

                            </Box>
                            <br />
                            <Typography variant="h5" gutterBottom>
                                {movie.Plot}
                            </Typography>
                            <Grid container>
                                <Grid item>
                                    <br />
                                    <Typography variant="subtitle1" gutterBottom>Top Cast</Typography>
                                    {movie.Actors.split(',').map((actor: string) => (
                                        <Box key={actor} display={"flex"}>
                                            <Typography color="textPrimary" variant="body1">{actor}</Typography>
                                        </Box>
                                    ))}
                                    <br />
                                    <ButtonGroup size="large" variant="outlined">
                                        <Button onClick={viewTrailer}>
                                            View Trailer
                                        </Button>
                                        <Button onClick={addToWatchList}>
                                            Add to watchlist
                                        </Button>
                                    </ButtonGroup>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={2}>
                            <Box className={classes.posterContainer}>
                                <img
                                    src={`${movie.Poster}`}
                                    className={classes.poster}
                                    alt={movie.Title}
                                />
                            </Box>
                        </Grid>
                    </Grid>
                </BaseCard>
                {movies.length > 0 ? (<Box mt={2}>
                    <Typography variant="h4" style={{ color: theme.palette.background.paper, fontWeight: 'bold' }} >Suggested for you:</Typography>
                    <Movies movies={movies} visible={movies.length > 10 ? 10 : movies.length - 2} scroll={1} />
                </Box>) : (<></>)}
            </Box>
        </>
    )
};

export default Detail;



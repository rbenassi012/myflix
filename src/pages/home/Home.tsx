import { useEffect, useState } from 'react';

// third part
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate, useSearchParams } from 'react-router-dom';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Box, Typography } from '@mui/material';

// project imports
import Navbar from '@myflix/components/Navbar';
import FeaturedMovie from '@myflix/components/FeaturedMovie';
import Movies from '@myflix/components/Movies'
import { FEATURED_MOVIES_MAPPING, FEATURED_MOVIE_TITE } from './const'


import { DEFAULT_NAVBAR_OPTIONS, PAGE_NOT_FOUND_PATH } from '@myflix/routes/consts';
import { getMovieByTitle, getFeaturedMovies, searchMovies } from '@myflix/reducers/omdb';



// ================================|| HOME ||================================ //



const Home = () => {
  const theme = useTheme();
  let [searchParams] = useSearchParams();
  const [isSearching, setIsSearching] = useState(false);

  const navigate = useNavigate();

  const dispatch = useDispatch();


  useEffect(() => {
    const query = searchParams.get("search")
    if (query !== "" && query !== null) {
      dispatch(getMovieByTitle(query));
      dispatch(searchMovies({ query: query, pages: 1 }))
      setIsSearching(true)
    } else {
      dispatch(getMovieByTitle(FEATURED_MOVIE_TITE));
      FEATURED_MOVIES_MAPPING.map(fmovie => dispatch(getFeaturedMovies({ query: fmovie.query, pages: 1 })));
      setIsSearching(false)
    }

  }, [searchParams]);


  const featuredMovie = useSelector(state => state.omdb.featuredMovie)
  const featuredMovieLoading = useSelector(state => state.omdb.featuredMovieLoading)
  const featuredMovieError = useSelector(state => state.omdb.featuredMovieError)
  const featuredMovies = useSelector(state => state.omdb.featuredMovies)
  const movies = useSelector(state => state.omdb.movies)

  useEffect(() => {
    if (featuredMovieError) {
      navigate(PAGE_NOT_FOUND_PATH)
    }
  }, [featuredMovieError])

  return (
    <>
      <Navbar
        settings={DEFAULT_NAVBAR_OPTIONS}
        goToHome={false}
      />

      <FeaturedMovie isLoading={featuredMovieLoading} movie={featuredMovie} />

      <Box p={3} style={{ clear: "both" }}>

        {!isSearching && FEATURED_MOVIES_MAPPING.map((fMovies: any) => {
          if (featuredMovies[fMovies.query] === undefined) return null;
          return (<Box mt={2} key={fMovies.query}>
            <Typography variant="h4" style={{ color: theme.palette.background.paper, fontWeight: 'bold' }}>{fMovies.label}</Typography>
            <Movies movies={featuredMovies[fMovies.query]} visible={featuredMovies[fMovies.query].length > 10 ? 10 : featuredMovies[fMovies.query].length - 2} scroll={1} />
          </Box>)
        })}

        {movies.length > 0 ? (<Box mt={2}>
          <Typography variant="h4" style={{ color: theme.palette.background.paper, fontWeight: 'bold' }} >Suggested for you:</Typography>
          <Movies movies={movies} visible={movies.length > 10 ? 10 : movies.length - 2} scroll={1} />
        </Box>) : (<></>)}
      </Box>
    </>
  )
};

export default Home;
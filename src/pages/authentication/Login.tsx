// material-ui
import { Grid } from '@mui/material';

// project import
import AuthLogin from './auth-forms/AuthLogin';
import AuthWrapper from './AuthWrapper';

// ================================|| LOGIN ||================================ //

interface LoginProps  {
  redirectTo: string;
  isLogged: boolean;
}

const Login = (props: LoginProps) => {
  return (
  <AuthWrapper>
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <AuthLogin {...props} />
      </Grid>
    </Grid>
  </AuthWrapper>
)};

export default Login;
// material-ui
import { Box, Grid } from '@mui/material';

// project import
import AuthCard from './AuthCard';

// assets
import Teaser from '../../components/Teaser';

// ==============================|| AUTHENTICATION - WRAPPER ||============================== //

interface AuthWrapperProps {
    children: JSX.Element
}

const AuthWrapper = (props: AuthWrapperProps) => {
    const { children } = props;
    return (<Box sx={{ minHeight: '100vh' }}>
        <Grid
            container
            direction="column"
            justifyContent="flex-end"
            sx={{
                minHeight: '100vh'
            }}
        >
            <Grid item xs={12} sx={{ ml: 3, mt: 3 }}  textAlign={"center"}>
                <Teaser />
            </Grid>
            <Grid item xs={12}>
                <Grid
                    item
                    xs={12}
                    container
                    justifyContent="center"
                    alignItems="center"
                    sx={{ minHeight: { xs: 'calc(100vh - 134px)', md: 'calc(100vh - 212px)' } }}
                >
                    <Grid item>
                        <AuthCard>{children}</AuthCard>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    </Box>);
};


export default AuthWrapper;
// material-ui
import { ComponentsProps } from '@mui/material';
import Box from '@mui/material/Box';

// project import
import BaseCard from '@myflix/components/BaseCard';

// ==============================|| AUTHENTICATION - CARD WRAPPER ||============================== //

interface AuthCardProps {
    children: JSX.Element,
    other?: ComponentsProps
}

const AuthCard = (props: AuthCardProps) => {
    const { children, other } = props;
    return (
        <BaseCard
            sx={{
                maxWidth: { xs: 400, lg: 475 },
                margin: { xs: 2.5, md: 3 },
                '& > *': {
                    flexGrow: 1,
                    flexBasis: '50%'
                }
            }}
            content={true}
            {...other}
        >
            <Box sx={{ p: { xs: 2, sm: 3, md: 4, xl: 5 } }}>{children}</Box>
        </BaseCard>
    )
};


export default AuthCard;
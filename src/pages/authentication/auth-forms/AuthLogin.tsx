import React from 'react';

// third party
import { Formik, FormikValues } from 'formik';
import { useNavigate } from 'react-router-dom';
import { useDispatch} from 'react-redux'

// material-ui
import {
  Button,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Stack,
} from '@mui/material';


// project import
import AnimatedButton from '@myflix/components/AnimatedButton';

// assets
import { VisibilityOutlined, VisibilityOffOutlined } from '@material-ui/icons';

import { setLoginStatus } from '@myflix/reducers/authentication';


interface AuthLoginProps {
  redirectTo: string,
  isLogged: boolean,
}

const AuthLogin = (props: AuthLoginProps) => {
  const {redirectTo, isLogged} = props;
  const navigate = useNavigate()
  const dispatch = useDispatch();
  const [showPassword, setShowPassword] = React.useState(false);


  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };


  const handleMouseDownPassword = (event: any) => {
    if (event !== undefined) {
      event.preventDefault();
    }
  };

  const handleLogin = () => {
    dispatch(setLoginStatus("true"))
    navigate(redirectTo)
  }

  return (
    <>
      {!isLogged ? (<Formik
        initialValues={{
          email: '',
          password: '',
          submit: null
        }}
        validate={(values) => {
          const errors = {} as FormikValues;
          if (!values.email){
            errors.email = "Required"
          } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
            errors.email = "Invalid email address"
          }

          if (!values.password) {
            errors.password = "Missing password"
          }
        
        }}
        onSubmit={() => handleLogin()}
      >
        {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values }) => (
          <form noValidate onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="email-login">User</InputLabel>
                  <OutlinedInput
                    id="email"
                    type="email"
                    value={values.email}
                    name="email"
                    placeholder="Enter your email"
                    fullWidth
                    size="small"
                    autoComplete="email"
                    error={Boolean(touched.email && errors.email)}
                    onBlur={handleBlur}
                    onChange={handleChange}
                  />
                  {touched.email && errors.email && (
                    <FormHelperText error id="standard-weight-helper-text-email-login">
                      {errors.email}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="password-login">Password</InputLabel>
                  <OutlinedInput
                    id="-password-login"
                    type={showPassword ? 'text' : 'password'}
                    value={values.password}
                    name="password"
                    fullWidth
                    size="small"
                    error={Boolean(touched.password && errors.password)}
                    autoComplete="current-password"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                          size="large"
                        >
                          {showPassword ? <VisibilityOutlined /> : <VisibilityOffOutlined />}
                        </IconButton>
                      </InputAdornment>
                    }
                    placeholder="Enter password"
                  />
                  {touched.password && errors.password && (
                    <FormHelperText error id="standard-weight-helper-text-password-login">
                      {errors.password}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>

              {errors.submit && (
                <Grid item xs={12}>
                  <FormHelperText error>{errors.submit}</FormHelperText>
                </Grid>
              )}
              <Grid item xs={12}>
                <AnimatedButton>
                  <Button disabled={isSubmitting} fullWidth size="large" type="submit" variant="contained" color="primary">
                    Login
                  </Button>
                </AnimatedButton>
              </Grid>
            </Grid>
          </form>
        )}
      </Formik>)
        :
        (<></>)
      }</>)
};

export default AuthLogin;
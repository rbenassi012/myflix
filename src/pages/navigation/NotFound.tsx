// material-ui
import { useTheme } from '@mui/material/styles';
import { Grid, Box, Typography } from '@mui/material';
import Button from '@material-ui/core/Button';
import AnimatedButton from '../../components/AnimatedButton';

// navigation
import { useNavigate } from 'react-router-dom';
import { HOME_PAGE_PATH } from '../../routes/consts';

// assets
import Teaser from '../../components/Teaser';



// ================================|| NOT FOUND ||================================ //


const NotFound = () => {
    const theme = useTheme();
    const navigate = useNavigate()
    return (
        <Box sx={{ minHeight: '100vh' }}>
            <Grid
                container
                direction="column"
                justifyContent="flex-end"
                sx={{
                    minHeight: '100vh'
                }}
            >
                <Grid item xs={12} sx={{ ml: 3, mt: 3 }} textAlign={"center"}>
                    <Teaser />
                </Grid>
                <Grid item xs={12}>
                    <Grid
                        item
                        xs={12}
                        container
                        direction={"column"}
                        justifyContent="center"
                        alignItems="center"
                        sx={{ minHeight: { xs: 'calc(100vh - 134px)', md: 'calc(100vh - 212px)' } }}
                    >
                        <Grid item>
                            <Box mt={-15}>
                                <Typography color={theme.palette.getContrastText(theme.palette.background.default)} variant="h3">
                                    AWWW... We've looked everywhere but nothing found!
                                </Typography>
                            </Box>
                        </Grid>
                        <Grid item>
                            <Box width={400} mt={5}>
                                <AnimatedButton>
                                    <Button 
                                        fullWidth 
                                        size="large" 
                                        type="submit" 
                                        variant="contained" 
                                        color="primary"
                                        onClick={() => navigate(HOME_PAGE_PATH)}
                                    >
                                        HOME
                                    </Button>
                                </AnimatedButton>
                            </Box>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Box>
    )
};

export default NotFound;
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import omdbClient from '@myflix/services/omdb/omdb';

export const searchMovies = createAsyncThunk('omdb/searchMovies', async ({query, pages}) => {
  const response = await omdbClient.searchMovies(query, pages);
  return response;
});

export const getMovieDetails = createAsyncThunk('omdb/getMovieDetails', async (imdbID) => {
  const response = await omdbClient.getMovieDetails(imdbID);
  return response;
});

export const getMovieByTitle = createAsyncThunk('omdb/featuredMovie', async (title) => {
  const response = await omdbClient.getMovieByTitle(title);
  return response;
});

export const getFeaturedMovies = createAsyncThunk('omdb/featuredMovies', async ({query, pages}) => {
  const response = await omdbClient.searchMovies(query, pages);
  return response;
});

const omdbSlice = createSlice({
  name: 'omdb',
  initialState: {
    movies: [],
    movieDetails: null,
    featuredMovie: null,
    featuredMovieLoading: false,
    featuredMovieError: false,
    featuredMovies: {},
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(searchMovies.pending, (state) => {
        state.movies = []
      })
      .addCase(searchMovies.fulfilled, (state, action) => {
        state.movies = action.payload;
      })
      .addCase(getMovieDetails.fulfilled, (state, action) => {
        state.movieDetails = action.payload;
      })
      .addCase(getMovieByTitle.pending, (state) => {
        state.featuredMovieLoading = true;
        state.featuredMovieError = false;
        state.featuredMovie = null;
      })
      .addCase(getMovieByTitle.fulfilled, (state, action) => {
        state.featuredMovieLoading = false;
        state.featuredMovie = action.payload;
        state.featuredMovieError = false;
      })
      .addCase(getMovieByTitle.rejected, (state) => {
        state.featuredMovieLoading = false;
        state.featuredMovie = null;
        state.featuredMovieError = true
      })
      .addCase(getFeaturedMovies.fulfilled, (state, action) => {
        state.featuredMovies[action.meta.arg.query] = action.payload;
      })
  },
});

export default omdbSlice.reducer;

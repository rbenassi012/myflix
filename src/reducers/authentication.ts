import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import authenticationClient from "../services/authentication/authentication";


export const checkLoginStatus = createAsyncThunk('authentication/checkLoginStatus', async () => {
    const response = await authenticationClient.checkLoginStatus();
    return response;
});

export const setLoginStatus = createAsyncThunk('authentication/setLoginStatus', async (status) => {
    const response = await authenticationClient.setLoginStatus(status);
    return response;
});

const authenticationSlice = createSlice({
    name: 'authentication',
    initialState: {
        isLogged: false,
    },
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(checkLoginStatus.fulfilled, (state, action) => {
                state.isLogged = action.payload
            })
            .addCase(setLoginStatus.fulfilled, (state, action) => {
                state.isLogged = action.meta.arg
            })
    },
});

export default authenticationSlice.reducer;